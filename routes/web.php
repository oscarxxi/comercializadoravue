<?php

use Illuminate\Support\Facades\Route;
use App\Models\empresa;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $empresa=empresa::first();
    return view('welcome', ['empresa'=>$empresa]);
});
