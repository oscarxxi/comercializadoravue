<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->id();
            $table->string('number_id');
            $table->integer('business_category_id');
            $table->string('address');
            $table->string('name');
            $table->integer('country_id');
            $table->integer('state_id');
            $table->integer('city_id');
            $table->string('zip_code');
            $table->string('favicon');
            $table->string('logo');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa');
    }
}
