<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('images/'.$empresa['favicon']) }}" type="image/x-icon">
    <meta name="csrf-token" value="{{ csrf_token() }}" />

    <title>{{$empresa['name']}}</title>

    <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet" />
</head>

<body>
    <div id="app">
        <div class="container-fluid">
            <example-component  
                :empresa="{{$empresa}}"/>
        </div>
    </div>
    <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
</body>

</html>